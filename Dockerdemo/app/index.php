<?php
$servername = "mysql";
$username = "my_user";
$password = "my_password";
$dbname = "my_database";

// Tạo kết nối
$conn = new mysqli($servername, $username, $password, $dbname);
// Kiểm tra kết nối
if ($conn->connect_error) {
    die("Kết nối thất bại: " . $conn->connect_error);
}
echo "Kết nối thành công đến cơ sở dữ liệu MySQL<br>";

// Hiển thị dữ liệu
$sql_select = "SELECT name, email, age FROM user";
$result = $conn->query($sql_select);

if ($result->num_rows > 0) {
    echo "<table border='1'><tr><th>Name</th><th>Email</th><th>Age</th></tr>";
    while($row = $result->fetch_assoc()) {
        echo "<tr><td>" . $row["name"] . "</td><td>" . $row["email"] . "</td><td>" . $row["age"] . "</td></tr>";
    }
    echo "</table>";
} else {
    echo "0 kết quả";
}

$conn->close();
echo "</br>";
phpinfo();
?>

